<?php

/**
 * @file
 * Defines entity class and controller class for embedded MongoDB documents
 *
 * Due to limitations in the current version of MongoDB, an embedded entity
 * can only be stored one level down.
 *
 * When using embedded entities, hook_entity_info requires a few extra
 * parameters:
 * - 'entity keys]['parent': the class field that stores the _id for the
 * parent document.
 * - 'collection': collection must be explicitly set to the parent document's
 * collection name
 * - 'collection field': the field of the parent document that stores the
 * embedded documents.
 */

/**
 * A controller class for entities stored as subdocuments in a collection.
 */
class MongoEmbeddedEntityController extends MongoEntityController {

  /**
   * {@inheritdoc}
   */
  public function __construct($entity_type) {
    parent::__construct($entity_type);

    $this->parentKey = $this->entityInfo['entity keys']['parent'];
    $this->collectionField = $this->entityInfo['collection field'];

  }

  /**
   * {@inheritdoc}
   */
  public function load($ids = array(), $conditions = array()) {

    $entity_class = $this->entityInfo['entity class'];

    $collection = mongodb_collection($this->collectionName);

    // @todo convert ids to int
    if ($ids) {
      $ids = array_map('intval', $ids);
      $query = array("{$this->collectionField}._id" => array('$in' => $ids));
      $projection = array($this->collectionField => array('$elemMatch' => array('_id' => array('$in' => $ids))));
    }
    elseif ($conditions) {
      $query = $conditions;
      $projection = array();
      foreach ($conditions as $key => $value) {

        // @todo check if is array
        $fields = explode('.', $key);
        if ($fields[0] == $this->collectionField) {
          array_shift($fields);
          if (is_array($value)) {
            $projection = array($this->collectionField => array('$elemMatch' => array(implode('.', $fields) => array('$in' => $value))));
          }
          else {
            $projection = array($this->collectionField => array('$elemMatch' => array(implode('.', $fields) => $value)));
          }
        }

      }
    }

    // @todo if empty projection
    if (empty($projection)) {
    }

    // Add embedded fields to projection
    if (isset($this->entityInfo['embedded fields'])) {
      foreach ($this->entityInfo['embedded fields'] as $source => $target) {
        $projection += array($source => 1);
      }
    }

    $cursor = $collection->find($query, $projection);
    $entities = array();

    foreach ($cursor as $document) {
      if (!empty($document[$this->collectionField])) {
        $parent_id = $document['_id'];
        foreach ($document[$this->collectionField] as $subdocument) {
          $id = $subdocument['_id'];
          $subdocument[$this->parentKey] = $parent_id;

          if (isset($this->entityInfo['embedded fields'])) {
            foreach ($this->entityInfo['embedded fields'] as $source => $target) {
              $subdocument[$target] = $document[$source];
            }
          }

          $entities[$id] = $this->buildEntity($subdocument);
        }
      }
    }

    return $entities;

  }

  /**
   * {@inheritdoc}
   */
  public function save($entity) {

    $document = $this->buildDocument($entity);

    $collection = mongodb_collection($this->collectionName);

    $this->invoke('presave', $entity);

    if ($entity->is_new) {
      // @todo if collection field doesn't exist, it won't be created.
      $collection->update(
        array('_id' => $entity->{$this->parentKey}),
        array('$push' => array($this->collectionField => $document))
      );
      $this->invoke('insert', $entity);
      $return = SAVED_NEW;
    }
    else {
      $collection->update(
        array('_id' => $entity->{$this->parentKey}, "{$this->collectionField}._id" => $entity->_id),
        array('$set' => array("{$this->collectionField}.$" => $document))
      );
      $this->invoke('update', $entity);
      $return = SAVED_UPDATED;
    }

    unset($entity->is_new);
    unset($entity->original);
    return $return;

  }

  /**
   * {@inheritdoc}
   */
  public function delete($ids) {

    $ids = array_map('intval', $ids);

    $collection = mongodb_collection($this->entityInfo['collection']);
    $collection->update(
      array(),
      array(
        '$pull' => array(
          $this->collectionField => array(
            '_id' => array('$in' => $ids),
          ),
        ),
      ),
      array('multiple' => TRUE)
    );

  }

}
