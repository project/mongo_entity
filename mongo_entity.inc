<?php

/**
 * @file
 * Defines entity class and controller class for MongoDB documents
 */

/**
 * A class for MongoDB backed entities
 */
class MongoEntity extends Entity {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $values, $entity_type = NULL) {

    if (empty($entity_type)) {
      throw new Exception('Cannot create an instance of Entity without a specified entity type.');
    }
    $this->entityType = $entity_type;
    $this->setUp();

    // Set initial values.
    foreach ($values as $key => $value) {
      if (isset($this->entityInfo['embedded entities'][$key])) {
        // Convert subdocuments to entities.
        $embedded_entity_type = $this->entityInfo['embedded entities'][$key];
        $embedded_entity_info = entity_get_info($embedded_entity_type);
        $parent_key = $embedded_entity_info['entity keys']['parent'];
        $controller = entity_get_controller($embedded_entity_type);
        foreach ($values[$key] as $document) {
          // @todo _id may not be set yet, if this is a new entity
          $document[$parent_key] = $values['_id'];

          // Copy other values from the parent to the embedded child.
          if (isset($embedded_entity_info['embedded fields'])) {
            foreach ($embedded_entity_info['embedded fields'] as $source => $target) {
              $document[$target] = $values[$source];
            }
          }

          $this->{$key}[] = $controller->buildEntity($document);
        }
      }
      else {
        $this->{$key} = $value;
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->labelKey = isset($this->entityInfo['entity keys']['label']) ? $this->entityInfo['entity keys']['label'] : NULL;
  }

  /**
   * A convenience function for entity forms. This may be removed in the future.
   * @todo Would it make more sense to put this in MongoEntityController?
   */
  public function hook_form($form, &$form_state) {

    // @todo don't show name field if no name
    // @todo check why nameKey isn't populated
    if (!empty($this->labelKey)) {
      $form[$this->labelKey] = array(
        '#type' => 'textfield',
        '#title' => t('Name'),
        '#default_value' => isset($this->{$this->labelKey}) ? $this->{$this->labelKey} : '',
        '#weight' => -100,
      );
    }

    field_attach_form($this->entityType, $this, $form, $form_state);

    $form['actions'] = array(
      '#type' => 'actions',
      '#weight' => 100,
    );
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Submit',
    );

    $form_state['entity'] = $this;

    return $form;

  }

  /**
   * A convenience function for validating entity forms.
   */
  public function hook_form_validate($form, $form_state) {
    field_attach_validate($this->entityType, $this, $form, $form_state);
  }

  /**
   * A convenience function for submitting entity forms.
   */
  public function hook_form_submit($form, $form_state) {

    entity_form_submit_build_entity($this->entityType, $this, $form, $form_state);

    $this->save();
    // @todo redirect
  }

}

/**
 * A controller for CRUD entity operations with MongoDB.
 *
 * @todo Consider overriding DrupalDefaultEntityController instead
 */
class MongoEntityController extends EntityAPIController {

  /**
   * A convenience method for MongoEntityControllers to provide menu callbacks.
   */
  public static function hook_menu() {
    return array();
  }

  /**
   * {@inheritdoc}
   */
  public function __construct($entity_type) {

    parent::__construct($entity_type);

    $this->nameKey = isset($this->entityInfo['entity keys']['name']) ? $this->entityInfo['entity keys']['name'] : NULL;
    $this->bundleKey = isset($this->entityInfo['entity keys']['bundle']) ? $this->entityInfo['entity keys']['bundle'] : NULL;
    $this->labelKey = isset($this->entityInfo['entity keys']['label']) ? $this->entityInfo['entity keys']['label'] : NULL;
    $this->collectionName = $this->entityInfo['collection'];

  }

  /**
   * {@inheritdoc}
   */
  public function buildQuery($ids, $conditions = array(), $revision_id = FALSE) {
    // @todo fix MongoEntityLoader
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function load($ids = array(), $conditions = array()) {

    $entities = array();

    // Create a new variable which is either a prepared version of the $ids
    // array for later comparison with the entity cache, or FALSE if no $ids
    // were passed. The $ids array is reduced as items are loaded from cache,
    // and we need to know if it's empty for this reason to avoid querying the
    // database when all requested entities are loaded from cache.
    if (!empty($ids)) {
      $ids = array_map('intval', array_filter($ids));
    }

    $passed_ids = is_array($ids) ? array_flip($ids) : FALSE;

    // Try to load entities from the static cache.
    if ($this->cache) {
      $entities = $this->cacheGet($ids, $conditions);
      // If any entities were loaded, remove them from the ids still to load.
      if ($passed_ids) {
        $ids = array_keys(array_diff_key($passed_ids, $entities));
      }
    }

    if (!$this->cacheComplete) {

      $queried_entities = array();

      $collection = mongodb_collection($this->collectionName);

      if ($passed_ids === FALSE && !$conditions) {
        $cursor = $collection->find();
      }
      else {

        $find = array();

        if (!empty($ids)) {
          if (count($ids) == 1) {
            $find['_id'] = current($ids);
          }
          else {
            $find['_id']['$in'] = $ids;
          }
        }
        elseif (!empty($conditions)) {
          foreach ($conditions as $key => $value) {
            // @todo better typing of properties
            $find[$key] = $value;
          }
        }

        if (!empty($find)) {
          $cursor = $collection->find($find);
        }

      }

      if (isset($cursor)) {
        foreach ($cursor as $document) {
          $id = $document['_id'];
          $queried_entities[$id] = $this->buildEntity($document);
        }
      }

    }

    // Pass all entities loaded from the database through $this->attachLoad(),
    // which attaches fields (if supported by the entity type) and calls the
    // entity type specific load callback, for example hook_node_load().
    if (!empty($queried_entities)) {
      $this->attachLoad($queried_entities);
      $entities += $queried_entities;
    }

    if ($this->cache) {
      // Add entities to the cache.
      if (!empty($queried_entities)) {
        $this->cacheSet($queried_entities);

        // Remember if we have cached all entities now.
        if (!$conditions && $ids === FALSE) {
          $this->cacheComplete = TRUE;
        }
      }
    }

    // Ensure that the returned array is ordered the same as the original
    // $ids array if this was passed in and remove any invalid ids.
    if ($passed_ids && $passed_ids = array_intersect_key($passed_ids, $entities)) {
      foreach ($passed_ids as $id => $value) {
        $passed_ids[$id] = $entities[$id];
      }
      $entities = $passed_ids;
    }

    return $entities;

  }

  /**
   * {@inheritdoc}
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {

    list($entity_id, $revision_id, $bundle) = entity_extract_ids($this->entityType, $entity);

    $this->invoke('presave', $entity);

    // Create a new object.
    $document = $this->buildDocument($entity);

    // Save the object.
    $collection = mongodb_collection($this->collectionName);
    $collection->save($document, array('w' => 1));

    $this->resetCache(array($entity->_id));
    if ($entity->is_new) {
      $this->invoke('insert', $entity);
      $return = SAVED_NEW;

    }
    else {
      $this->invoke('update', $entity);
      $return = SAVED_UPDATED;
    }

    unset($entity->is_new);
    unset($entity->original);
    return $return;

  }

  /**
   * Implements EntityAPIControllerInterface.
   */
  public function delete($ids) {

    $ids = array_map('intval', $ids);

    $collection = mongodb_collection($this->collectionName);
    $collection->remove(array('_id' => array('$in' => $ids)));

    // Reset the cache as soon as the changes have been applied.
    $this->resetCache($ids);

    // @todo load entities (seriously?) and invoke delete for all entities
  }

  /**
   * Helper function to convert entity data to MongoDB document structure.
   *
   * When saving a MongoEntity to the collection, all of the entity's public
   * variables (found with get_class_vars) are written to the document, as well
   * as a reduced array of its fields (borrowed from
   * mongo_field_storage.module). A unique numeric _id is generated and if the
   * entity has a label key, that property is saved as well.
   */
  public function buildDocument($entity) {

    $entity->is_new = !empty($entity->is_new) || empty($entity->_id);

    list($entity_id, $revision_id, $bundle) = entity_extract_ids($this->entityType, $entity);
    $bundle = $bundle ? $bundle : $this->entityType;

    // Generate a (numeric) id for new entities.
    // Drupal requires (or strongly recommendes) numeric ids for entities,
    // preventing us from using Mongo's standard ObjectId.
    if ($entity->is_new) {

      $counter_name = "mongo_entity_{$this->entityType}_counter";

      // @todo handle transaction exceptions
      $transaction = db_transaction();
      $counter = variable_get($counter_name, 0);
      $entity->_id = ++$counter;

      variable_set($counter_name, $counter);

    }

    // Get the list of fields for this entity type and bundle.
    $fields = field_info_instances($this->entityType, $bundle);

    // Get the list of defined class properties, to store with this entity.
    $properties = get_class_vars(get_class($entity));

    // Use property info to cast values before saving.

    $property_info = array();

    $entity_property_info = entity_get_property_info($this->entityType);
    if (isset($entity_property_info['properties'])) {
      $property_info = array_merge($property_info, $entity_property_info['properties']);
    }
    if (isset($entity_property_info['bundles'][$bundle]['properties'])) {
      $property_info = array_merge($property_info, $entity_property_info['bundles'][$bundle]['properties']);
    }

    // Create a new object.
    $document = new stdClass();

    // @todo nameKey is overwriting _id
    if ($this->nameKey) {
      $document->{$this->nameKey} = $entity->{$this->nameKey};
    }
    if ($this->bundleKey) {
      $document->{$this->bundleKey} = $entity->{$this->bundleKey};
    }
    if ($this->labelKey) {
      $document->{$this->labelKey} = $entity->{$this->labelKey};
    }

    // Write properites.
    if (!empty($properties)) {
      foreach (array_keys($properties) as $key) {

        // @todo Check if the value is set? Unset it if it isn't
        // If this is_a MongoEntity, convert it to an array. The
        // MongoEmbeddedEntity concept may be flawed, so this may be removed
        // in the future, possibly to be replaced by more entity properties.
        if (isset($this->entityInfo['embedded entities'][$key])) {
          $embedded_entity_type = $this->entityInfo['embedded entities'][$key];
          $controller = entity_get_controller($embedded_entity_type);
          $document->$key = array_map(
            function ($item) use ($controller) {
              if (is_a($item, 'MongoEntity')) {
                return $controller->buildDocument($item);
              }
              else {
                return $item;
              }
            },
            $entity->$key
          );
        }
        else {
          if (isset($entity->$key)) {
            // If the class property has a corresponding property info entry,
            // use the 'type' attribute to cast this value before writing
            // to MongoDB.
            $type = isset($property_info[$key]['type']) ? $property_info[$key]['type'] : NULL;
            switch ($type) {
              case 'text':
                $document->$key = strval($entity->$key);
                break;
              case 'integer':
                $document->$key = intval($entity->$key);
                break;
              case 'decimal':
                $document->$key = floatval($entity->$key);
                break;
              case 'boolean':
                $document->$key = $entity->$key ? TRUE : FALSE;
                break;
              default:
                $document->$key = $entity->$key;
                break;
            }
          }
        }
      }
    }

    // Write fields
    // From mongodb_field_storage.module:
    // Add the fieldapi fields to the new object.
    if (!empty($fields)) {
      foreach ($fields as $field_name => $field) {
        $field = field_info_field($field_name);
        $field_values = array();
        if (!empty($entity->$field_name)) {
          foreach ($entity->$field_name as $language => $values) {
            // According to field.test, we should not save anything for NULL.
            if (!empty($values[0])) {
              if ($field['cardinality'] == 1) {
                foreach ($values[0] as $column_name => $column_value) {
                  if (isset($field['columns'][$column_name])) {
                    $field_values[$column_name] = _mongo_entity_value($field['columns'][$column_name]['type'], $column_value);
                  }
                  if ($language != LANGUAGE_NONE) {
                    $field_values['_language'] = $language;
                  }
                }
              }
              else {
                // Collapse deltas.
                $values = array_values($values);
                if ($field['cardinality'] > 1 && count($values) > $field['cardinality']) {
                  throw new MongodbStorageException(t('Invalid delta for @field_name, not saving @entity_type @entity_id', array(
                    '@field_name' => $field_name,
                    '@entity_type' => $this->entityType,
                    '@entity_id' => $entity_id,
                  )));
                }
                foreach ($values as $delta => $column_values) {
                  $store_values = array();
                  foreach ($column_values as $column_name => $column_value) {
                    if (isset($field['columns'][$column_name])) {
                      $store_values[$column_name] = _mongo_entity_value($field['columns'][$column_name]['type'], $column_values[$column_name]);
                    }
                  }
                  // Collapse the field structure.
                  if ($language != LANGUAGE_NONE) {
                    $store_values['_language'] = $language;
                  }
                  $field_values[] = $store_values;
                }
              }
            }
          }
        }
        $document->$field_name = empty($field_values) ? NULL : $field_values;
      }
    }

    $document->_id = intval($entity->_id);

    return $document;

  }

  /**
   * Helper function to extract Mongo document to an entity.
   *
   * Mainly used to unravel the field structure into its language and delta keys
   * i.e. {field_example: 4} becomes field_example['und'][0]['value'] = 4;
   */
  public function buildEntity($document) {

    $entity_class = $this->entityInfo['entity class'];

    if ($this->bundleKey && isset($document[$this->bundleKey])) {
      $bundle = $document[$this->bundleKey];
    }
    else {
      $bundle = NULL;
    }

    $fields = field_info_instances($this->entityType, $bundle ? $bundle : $this->entityType);

    $properties = get_class_vars($entity_class);

    // From mongodb_field_structure.module:
    foreach ($fields as $field_name => $field) {
      $field = field_info_field($field_name);
      $field_values = array();

      if (!empty($document[$field_name])) {
        // Restore the field structure.
        if ($field['cardinality'] == 1) {
          $language = isset($document[$field_name]['_language']) ? $document[$field_name]['_language'] : LANGUAGE_NONE;
          unset($document[$field_name]['_language']);
          $field_values[$language][0] = $document[$field_name];
        }
        else {
          foreach ($document[$field_name] as $delta => $column_values) {
            $language = isset($column_values['_language']) ? $column_values['_language'] : LANGUAGE_NONE;
            unset($column_values['_language']);
            $field_values[$language][$delta] = $column_values;
          }
        }
      }
      $document[$field_name] = $field_values;
    }

    return new $entity_class($document, $this->entityType);

  }

  /**
   * Overridden to care about reverted bundle entities and to skip Rules.
   */
  public function invoke($hook, $entity) {

    if ($hook == 'delete') {
      // To ease figuring out whether this is a revert, make sure that the
      // entity status is updated in case the providing module has been
      // disabled.
      if (entity_has_status($this->entityType, $entity, ENTITY_IN_CODE) && !module_exists($entity->{$this->moduleKey})) {
        $entity->{$this->statusKey} = ENTITY_CUSTOM;
      }
      $is_revert = entity_has_status($this->entityType, $entity, ENTITY_IN_CODE);
    }

    // Do not use field attach API!

    // Invoke the hook.
    module_invoke_all($this->entityType . '_' . $hook, $entity);
    // Invoke the respective entity level hook.
    if ($hook == 'presave' || $hook == 'insert' || $hook == 'update' || $hook == 'delete') {
      // Paranoia. Don't let mongodb_field_storage execute its hook. Everyone else is AOK.
      foreach (module_implements('entity_' . $hook) as $module) {
        if ($module != 'mongodb_field_storage') {
          module_invoke($module, 'entity_' . $hook, $entity, $this->entityType);
        }
      }
    }

  }

  /**
   * Overridden.
   * @see DrupalDefaultEntityController::attachLoad()
   *
   * Changed to call type-specific hook with the entities keyed by name if they
   * have one.
   */
  protected function attachLoad(&$queried_entities, $revision_id = FALSE) {

    // Do not use field attach API!

    // Call hook_entity_load(), for every module except mongodb_field_storage
    foreach (module_implements('entity_load') as $module) {
      if ($module != 'mongodb_field_storage') {
        $function = $module . '_entity_load';
        $function($queried_entities, $this->entityType);
      }
    }
    // Call hook_TYPE_load(). The first argument for hook_TYPE_load() are
    // always the queried entities, followed by additional arguments set in
    // $this->hookLoadArguments.
    // For entities with a name key, pass the entities keyed by name to the
    // specific load hook.
    if ($this->nameKey != $this->idKey) {
      $entities_by_name = entity_key_array_by_property($queried_entities, $this->nameKey);
    }
    else {
      $entities_by_name = $queried_entities;
    }
    $args = array_merge(array($entities_by_name), $this->hookLoadArguments);
    foreach (module_implements($this->entityInfo['load hook']) as $module) {
      call_user_func_array($module . '_' . $this->entityInfo['load hook'], $args);
    }

  }

}
